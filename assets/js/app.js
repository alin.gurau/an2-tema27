/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.scss in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

require("bootstrap");

import Vue from "vue";

var ex = new Vue({
    el: "#exercise",
    delimiters: ["{*", "*}"],
    data: {
        name: "Alin Gurau",
        age: 26,
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdKjE3Y994pkmYSR4IvnJ3f_OHzz4zhderNfgfC88R5C5DZIdO",
    },
    methods: {
        randomFloat: function () {
            return Math.random() * (100 - 0) + 0;
        },
        nameDisplay: function () {
            return this.name;
        }
    }
});
